from flask import Flask, render_template, Response, request
import cv2
import numpy as np
import imutils
import torch
import torchvision
import threading
import time
from utils import preprocess
import torch.nn.functional as F
import torchvision.transforms as transforms
import os

# Change width or height for the video display
def pipeline(
    sensor_id=0,
    capture_width=300,
    capture_height=300,
    display_width=300,
    display_height=300,
    framerate=24,
    flip_method=0,
):
    return (
        "nvarguscamerasrc sensor_id=%d !"
        "video/x-raw(memory:NVMM), width=(int)%d, height=(int)%d, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )

# Get model
# Change the number of labels here
dataset_len_categories = 2
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = torchvision.models.resnet18()
model.fc = torch.nn.Linear(512, dataset_len_categories)
model.load_state_dict(torch.load('my_model.pth'))
model = model.to(DEVICE)
model.eval()

 #Remove old image
try:
    os.remove('image.jpg')
except:
    print("No image file")

# Parameters
thickness_text = 1
thickness = -1
fontScale = 0.8
font = cv2.FONT_HERSHEY_SIMPLEX
thickness_text = 1
fontScale_3 = 0.3
line_thickness = 2
light_green_color = (0,255,100)
black_color = (0,0,0)
scale_percent = 40

# Coordinates
text_org = (0, 20)
white_color = (255,255,255)
trying_to_predict_rect = (560, 5), (350, 35)

# Flask app
app = Flask(__name__)

video_capture = cv2.VideoCapture(pipeline(flip_method=0), cv2.CAP_GSTREAMER)

def show_camera():
    if video_capture.isOpened():
            while True:
                ret, frame = video_capture.read()
                
                # Preprocess frames
                preprocessed_frame = preprocess(frame)
                
                with torch.no_grad():
                    output = model(preprocessed_frame)
                    output = F.softmax(output, dim=1).detach().cpu().numpy().flatten()
                    
                    # Categories
                    cat_list = ['punsch_roll', 'chocolate_ball']

                    # Output to list
                    list_output = output.tolist()
                    
                    # Index of max value
                    max_value = list_output.index(max(list_output))
                    
                    # Få ut label
                    text = cat_list[max_value]

                    # Accuracy
                    acc = max(list_output)
                    
                    # Set your own value here based on how many percent you want it to have
                    if(acc>0.9):
                        
                        # Add more here if you have more labels being used in the model
                        # DelicatoBall
                        if(text == cat_list[1]):
                            frame = cv2.putText(frame, "Delicatoball", text_org,font, fontScale, light_green_color, thickness_text)
                            recept = cv2.imread("images/Delicatoballs_recipe.jpg", cv2.IMREAD_COLOR)
                            width = int(recept.shape[1] * scale_percent / 100)
                            height = int(recept.shape[0] * scale_percent / 100)
                            dsize = (width, height)
                            output = cv2.resize(recept, dsize)
                            cv2.imwrite('image.jpg', output)
                        # Punschrolls   
                        elif(text == cat_list[0]):
                            frame = cv2.putText(frame, "Punschroll", text_org,font, fontScale, light_green_color, thickness_text)
                            recept = cv2.imread("images/Punschrolls_recipe.jpg", cv2.IMREAD_COLOR)
                            width = int(recept.shape[1] * scale_percent / 100)
                            height = int(recept.shape[0] * scale_percent / 100)
                            dsize = (width, height)
                            output = cv2.resize(recept, dsize)
                            cv2.imwrite('image.jpg', output)
                        # If no detection
                        else:
                            frame = cv2.putText(frame, f'Trying to predict', text_org,font, fontScale, white_color, thickness_text)
                            
                      
                ret, buffer = cv2.imencode('.jpg', frame)
                
                frame = buffer.tobytes()
                yield (b'--frame\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

# Gets the current image based on the pastry being shown   
def image():
    while True:
        new_img = cv2.imread("image.jpg", cv2.IMREAD_COLOR)
        if new_img is not None:
            ret, buffer = cv2.imencode('.jpg', new_img)
            new_img = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + new_img + b'\r\n')
    
#Flask connections
@app.route('/show_image')
def show_image():
    return Response(image(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/video_feed')
def video_feed():
    return Response(show_camera(), 
                    mimetype='multipart/x-mixed-replace; boundary=frame')

app.run(host="0.0.0.0", port=80)
video_capture.release()
