# PastryDetector

## Description
A pastry detector that detects pastries with the help of the camera. The detected pastry's name is written on the screen and a recipe of the pastry is displayed.

## Table of Contents
- [Background](#background)
- [Implementation](#implementation)
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [API](#api)
- [Contributing](#contributing)
- [License](#license)

## Background
There is alot of different pastries out there in the world. It can be difficult to learn the names of all of them and to find good recipes on them. This program detects pastries that the model has learned and display their name and recipes for that pastry.

## Implementation
The program uses a model trained by Pytorch with a pretrained ResNet-18 model and Adam as a optimizer. The model can detect either a punschroll or a delicatoball, where each pastry has around 400 images in different angles, light levels and how much the pastry has been eaten.

The detector program runs on a flask server to run the camera and when it detects a pastry it shows the name of the pastry and displays a recipe.

The program was created on a [Jetank](https://www.waveshare.com/jetank-ai-kit.htm). Since it uses none of its unique features and uses a camera code from jetbot, it should work on an ordinary Jetson Nano.

## Requirements
* Jetson Nano
* Jetson Nano compatible camera
* Punschrolls, Delicatoballs or both. (Images works too)

Setup your Jetson Nano following Nvidias instructions from: https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit#intro

## Installation
- Clone the repository in to your jetson nano
- Install the different packages with: 
```
pip install %nameofAPI%
```
- If you want to use the Modeltrainer:
    - Install jupyter notebook with:
        ```
        pip install jupyterlab
        ```
    - clone the jetbot repository:
        ```
        git clone https://github.com/NVIDIA-AI-IOT/jetbot.git
        ```


## Usage
### Running the flask server:
```
- Open terminal
- Goto project files to .../Flask
- Run python3 flaskstart_main.py
- Open the adress that appears in a browser to see the result
```
Use the pastries (or images) to see the detection and the recipes.

### Running the model creator:
```
- Open jupyter notebook
- Upload the project and the jetbot file to the notebook
- Run the .ipynb file.
- install any missing packages if you don't have them in notebook
```
Here you can make a new model that detects new pastries. Create new labels in the code and start taking fotos of the pastry in different angles and light. Press the train button to let it train and the on the eval button to evaluate the model. Look at the values to see the result. When you are satisfied with the result, save the model under the Flask folder and modify the flaskstart_main.py file with the new label (marked with comments in the file).
## API
- [Numpy](https://numpy.org/)
- [Opencv](https://opencv.org/)
- [Flask](https://flask.palletsprojects.com/en/2.2.x/)
- [Pytorch](https://pytorch.org/)
- [Imutils](https://pypi.org/project/imutils/)
- [Traitlets](https://github.com/ipython/traitlets)
- [IPython](https://ipython.org/install.html)
- [IPywidgets](https://github.com/jupyter-widgets/ipywidgets)
- [Jetbot](https://github.com/NVIDIA-AI-IOT/jetbot)

## Contributing
You are welcome to clone the project and make small changes to the program.
You can also fork the project and add new features if you give credit to the author. 
Bigger changes should have an issue opened to discuss what changes you want to do.

## License
[MIT](docs/LICENSE.md)

